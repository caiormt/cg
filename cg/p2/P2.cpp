#include "geometry/MeshSweeper.h"
#include "P2.h"

MeshMap P2::_defaultMeshes;

inline void
P2::buildDefaultMeshes()
{
  _defaultMeshes["None"] = nullptr;
  _defaultMeshes["Box"] = GLGraphics3::box();
  _defaultMeshes["Sphere"] = GLGraphics3::sphere();
}

inline Primitive*
makePrimitive(MeshMapIterator mit)
{
  return new Primitive{mit->second, mit->first};
}

inline void
P2::buildScene()
{
  _current = _scene = Scene::makeUse(new Scene{ "Scene 1" });

  {
    auto camera = new Camera;
    Reference o = new SceneObject{ "Main Camera", *_scene };

    o->addComponent(camera);
    o->setParent(_scene->root());

    o = new SceneObject{ "Box 1", *_scene };
    o->setParent(_scene->root());
    o->addComponent(makePrimitive(_defaultMeshes.find("Box")));

    o = new SceneObject{ "Box 2", *_scene };
    o->setParent(_scene->root());
    o->addComponent(makePrimitive(_defaultMeshes.find("Box")));

    o = new SceneObject{ "Object 1", *_scene };
    o->setParent(_scene->root());

    { // nested
      auto ob = new SceneObject{ "Box 1", *_scene };
      ob->setParent(o);
      ob->addComponent(makePrimitive(_defaultMeshes.find("Box")));

      ob = new SceneObject{ "Object 1", *_scene };
      ob->setParent(o);
    }

    Camera::setCurrent(camera);
  }
}

void
P2::initialize()
{
  Application::loadShaders(_program, "shaders/p2.vs", "shaders/p2.fs");
  Assets::initialize();
  buildDefaultMeshes();
  buildScene();
  
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_POLYGON_OFFSET_FILL);
  glPolygonOffset(1.0f, 1.0f);
  glEnable(GL_LINE_SMOOTH);

  _renderer = new GLRenderer{ *_scene, &_program };
  _editor = new SceneEditor{ *_scene, &_program };
  _editor->setDefaultView((float)width() / (float)height());

  _program.use();
}

namespace ImGui
{
  void ShowDemoWindow(bool*);
}

static void
RecursivelyRenderHierarchy(SceneObject& node, SceneNode** current)
{
  for (auto const& child : node.children())
  {
    if (!child->drawable)
      continue;

    auto hasChildren = child->hasChildren();

    ImGuiTreeNodeFlags flag = hasChildren ?
      ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick :
      ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen;

    auto open = ImGui::TreeNodeEx(
      child,
      child == *current ? flag | ImGuiTreeNodeFlags_Selected : flag,
      child->name()
    );

    if (ImGui::IsItemClicked())
      (*current) = child;

    if (ImGui::BeginDragDropTarget())
    {
      if (const auto* payload = ImGui::AcceptDragDropPayload("setParent"))
      {
        if (auto source = dynamic_cast<SceneObject*>(*current))
        {
          auto recursive = false;
          for (auto parent = child->parent(); parent != nullptr && !recursive; parent = parent->parent())
            if (source == parent) recursive = true;

          if (!recursive)
            source->setParent(child);
        }
      }
      ImGui::EndDragDropTarget();
    }

    if (ImGui::BeginDragDropSource())
    {
      ImGui::Text("Changing parent of \"%s\"", child->name());
      ImGui::SetDragDropPayload("setParent", child.get(), sizeof(SceneObject*));
      ImGui::EndDragDropSource();
    }

    if (hasChildren && open)
    {
      RecursivelyRenderHierarchy(*child, current);
      ImGui::TreePop();
    }
  }
}

static inline void
AddSceneObjectToStruture(SceneNode& _current, SceneObject& object)
{
  if (auto current = dynamic_cast<Scene*>(&_current))
  {
    object.setParent(current->root());
  }
  else if (auto current = dynamic_cast<SceneObject*>(&_current))
  {
    object.setParent(current);
  }
  else
  {
    // PANIC
    delete &object;
  }
}

inline void
P2::hierarchyWindow()
{
  ImGui::Begin("Hierarchy");
  if (ImGui::Button("Create###object"))
    ImGui::OpenPopup("CreateObjectPopup");
  if (ImGui::BeginPopup("CreateObjectPopup"))
  {
    if (ImGui::MenuItem("Empty Object"))
    {
      auto name = "Object " + std::to_string(_current->nextObjectSeq());
      Reference newObject = new SceneObject{ name.c_str(), *_scene };
      AddSceneObjectToStruture(*_current, *newObject);
    }
    if (ImGui::BeginMenu("3D Object"))
    {
      if (ImGui::MenuItem("Box"))
      {
        auto name = "Box " + std::to_string(_current->nextBoxSeq());
        Reference newBox = new SceneObject{ name.c_str(), *_scene };
        newBox->addComponent(makePrimitive(_defaultMeshes.find("Box")));
        AddSceneObjectToStruture(*_current, *newBox);
      }
      if (ImGui::MenuItem("Sphere"))
      {
        auto name = "Sphere " + std::to_string(_current->nextSphereSeq());
        Reference newSphere = new SceneObject{ name.c_str(), *_scene };
        newSphere->addComponent(makePrimitive(_defaultMeshes.find("Sphere")));
        AddSceneObjectToStruture(*_current, *newSphere);
      }
      ImGui::EndMenu();
    }
    if (ImGui::MenuItem("Camera"))
    {
      Reference newCamera = new SceneObject{ "Camera", *_scene };
      newCamera->addComponent(new Camera);
      AddSceneObjectToStruture(*_current, *newCamera);
    }
    ImGui::EndPopup();
  }
  ImGui::Separator();

  ImGuiTreeNodeFlags flag{ ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick };
  auto open = ImGui::TreeNodeEx(
    _scene,
    _scene == _current ? flag | ImGuiTreeNodeFlags_Selected : flag,
    _scene->name()
  );

  if (ImGui::IsItemClicked())
    _current = _scene;

  if (ImGui::BeginDragDropTarget())
  {
    if (const auto* payload = ImGui::AcceptDragDropPayload("setParent"))
    {
      if (auto source = dynamic_cast<SceneObject*>(_current))
        source->setParent(_scene->root());
    }
    ImGui::EndDragDropTarget();
  }

  if (open)
  {
    RecursivelyRenderHierarchy(*_scene->root(), &_current);
    ImGui::TreePop();
  }

  ImGui::End();
}

namespace ImGui
{ // begin namespace ImGui

void
ObjectNameInput(NameableObject* object)
{
  const int bufferSize{128};
  static NameableObject* current;
  static char buffer[bufferSize];

  if (object != current)
  {
    strcpy_s(buffer, bufferSize, object->name());
    current = object;
  }
  if (ImGui::InputText("Name", buffer, bufferSize))
    object->setName(buffer);
}

inline bool
ColorEdit3(const char* label, Color& color)
{
  return ImGui::ColorEdit3(label, (float*)&color);
}

inline bool
DragVec3(const char* label, vec3f& v)
{
  return DragFloat3(label, (float*)&v, 0.1f, 0.0f, 0.0f, "%.2g");
}

void
TransformEdit(Transform* transform)
{
  vec3f temp;

  temp = transform->localPosition();
  if (ImGui::DragVec3("Position", temp))
    transform->setLocalPosition(temp);

  temp = transform->localEulerAngles();
  if (ImGui::DragVec3("Rotation", temp))
    transform->setLocalEulerAngles(temp);
  
  temp = transform->localScale();
  if (ImGui::DragVec3("Scale", temp))
  {
    for (auto index = 0; index < 3; index++) // Normalizing (do not allow scale less than 0.001)
      temp[index] = temp[index] >= 0.001f ? temp[index] : 0.001f;

    transform->setLocalScale(temp);
  }
}

} // end namespace ImGui

inline void
P2::sceneGui()
{
  auto scene = (Scene*)_current;

  ImGui::ObjectNameInput(_current);
  ImGui::Separator();
  if (ImGui::CollapsingHeader("Colors"))
  {
    ImGui::ColorEdit3("Background", scene->backgroundColor);
    ImGui::ColorEdit3("Ambient Light", scene->ambientLight);
  }
}

inline void
P2::inspectPrimitive(Primitive& primitive)
{
  char buffer[16];

  snprintf(buffer, 16, "%s", primitive.meshName());
  ImGui::InputText("Mesh", buffer, 16, ImGuiInputTextFlags_ReadOnly);
  if (ImGui::BeginDragDropTarget())
  {
    if (auto* payload = ImGui::AcceptDragDropPayload("PrimitiveMesh"))
    {
      auto mit = *(MeshMapIterator*)payload->Data;
      primitive.setMesh(mit->second, mit->first);
    }
    ImGui::EndDragDropTarget();
  }
  ImGui::SameLine();
  if (ImGui::Button("...###PrimitiveMesh"))
    ImGui::OpenPopup("PrimitiveMeshPopup");
  if (ImGui::BeginPopup("PrimitiveMeshPopup"))
  {
    auto& meshes = Assets::meshes();

    if (!meshes.empty())
    {
      for (auto mit = meshes.begin(); mit != meshes.end(); ++mit)
        if (ImGui::Selectable(mit->first.c_str()))
          primitive.setMesh(Assets::loadMesh(mit), mit->first);
      ImGui::Separator();
    }
    for (auto mit = _defaultMeshes.begin(); mit != _defaultMeshes.end(); ++mit)
      if (ImGui::Selectable(mit->first.c_str()))
        primitive.setMesh(mit->second, mit->first);
    ImGui::EndPopup();
  }
  ImGui::ColorEdit3("Mesh Color", (float*)&primitive.color);
}

void
P2::inspectCamera(Camera& camera)
{
  static const char* projectionNames[]{"Perspective", "Orthographic"};
  auto cp = camera.projectionType();

  if (ImGui::BeginCombo("Projection", projectionNames[cp]))
  {
    for (auto i = 0; i < IM_ARRAYSIZE(projectionNames); ++i)
    {
      auto selected = cp == i;

      if (ImGui::Selectable(projectionNames[i], selected))
        cp = (Camera::ProjectionType)i;
      if (selected)
        ImGui::SetItemDefaultFocus();
    }
    ImGui::EndCombo();
  }
  camera.setProjectionType(cp);
  if (cp == View3::Perspective)
  {
    auto fov = camera.viewAngle();

    if (ImGui::SliderFloat("View Angle",
      &fov,
      MIN_ANGLE,
      MAX_ANGLE,
      "%.0f deg",
      1.0f))
      camera.setViewAngle(fov <= MIN_ANGLE ? MIN_ANGLE : fov);
  }
  else
  {
    auto h = camera.height();

    if (ImGui::DragFloat("Height",
      &h,
      MIN_HEIGHT * 10.0f,
      MIN_HEIGHT,
      math::Limits<float>::inf()))
      camera.setHeight(h <= 0 ? MIN_HEIGHT : h);
  }

  float n;
  float f;

  camera.clippingPlanes(n, f);

  if (ImGui::DragFloatRange2("Clipping Planes",
    &n,
    &f,
    MIN_DEPTH,
    MIN_DEPTH,
    math::Limits<float>::inf(),
    "Near: %.2f",
    "Far: %.2f"))
  {
    if (n <= 0)
      n = MIN_DEPTH;
    if (f - n < MIN_DEPTH)
      f = n + MIN_DEPTH;
    camera.setClippingPlanes(n, f);
  }
}

inline void
P2::addComponentButton(SceneObject& object)
{
  if (ImGui::Button("Add Component"))
    ImGui::OpenPopup("AddComponentPopup");
  if (ImGui::BeginPopup("AddComponentPopup"))
  {
    if (ImGui::BeginMenu("Primitive"))
    {
      if (ImGui::MenuItem("Box"))
      {
        object.addComponent(makePrimitive(_defaultMeshes.find("Box")));
      }
      if (ImGui::MenuItem("Sphere"))
      {
        object.addComponent(makePrimitive(_defaultMeshes.find("Sphere")));
      }
      ImGui::EndMenu();
    }
    if (ImGui::MenuItem("Camera"))
    {
      object.addComponent(new Camera);
    }
    ImGui::EndPopup();
  }
}

inline void
P2::sceneObjectGui()
{
  auto object = (SceneObject*)_current;

  addComponentButton(*object);
  ImGui::Separator();
  ImGui::ObjectNameInput(object);
  ImGui::SameLine();
  ImGui::Checkbox("###visible", &object->visible);
  ImGui::Separator();
  
  if (ImGui::CollapsingHeader(object->transform()->typeName()))
    ImGui::TransformEdit(object->transform());

  for (const auto& component : object->components())
  {
    if (const auto p = dynamic_cast<Primitive*>(component.get()))
    {
      auto notDelete{ true };
      auto open = ImGui::CollapsingHeader(p->typeName(), &notDelete);

      if (!notDelete)
      {
        //object->removeComponent(p);
      }
      else if (open)
        inspectPrimitive(*p);
    }
    else if (const auto c = dynamic_cast<Camera*>(component.get()))
    {
      auto notDelete{ true };
      auto open = ImGui::CollapsingHeader(c->typeName(), &notDelete);

      if (!notDelete)
      {
        //object->removeComponent(c);
      }
      else if (open)
      {
        auto isCurrent = c == Camera::current();

        ImGui::Checkbox("Current", &isCurrent);
        Camera::setCurrent(isCurrent ? c : nullptr);
        inspectCamera(*c);
      }
    }
  }
}

inline void
P2::objectGui()
{
  if (_current == nullptr)
    return;
  if (dynamic_cast<SceneObject*>(_current))
  {
    sceneObjectGui();
    return;
  }
  if (dynamic_cast<Scene*>(_current))
    sceneGui();
}

inline void
P2::inspectorWindow()
{
  ImGui::Begin("Inspector");
  objectGui();
  ImGui::End();
}

inline void
P2::editorViewGui()
{
  if (ImGui::Button("Set Default View"))
    _editor->setDefaultView(float(width()) / float(height()));
  ImGui::Separator();

  auto t = _editor->camera()->transform();
  vec3f temp;

  temp = t->localPosition();
  if (ImGui::DragVec3("Position", temp))
    t->setLocalPosition(temp);
  temp = t->localEulerAngles();
  if (ImGui::DragVec3("Rotation", temp))
    t->setLocalEulerAngles(temp);
  inspectCamera(*_editor->camera());
  ImGui::Separator();
  ImGui::Checkbox("Show Ground", &_editor->showGround);
}

inline void
P2::assetsWindow()
{
  if (!_showAssets)
    return;
  ImGui::Begin("Assets");
  if (ImGui::CollapsingHeader("Meshes"))
  {
    auto& meshes = Assets::meshes();

    for (auto mit = meshes.begin(); mit != meshes.end(); ++mit)
    {
      auto meshName = mit->first.c_str();
      auto selected = false;

      ImGui::Selectable(meshName, &selected);
      if (ImGui::BeginDragDropSource())
      {
        Assets::loadMesh(mit);
        ImGui::Text(meshName);
        ImGui::SetDragDropPayload("PrimitiveMesh", &mit, sizeof(mit));
        ImGui::EndDragDropSource();
      }
    }
  }
  ImGui::Separator();
  if (ImGui::CollapsingHeader("Textures"))
  {
    // p3
  }
  ImGui::End();
}

inline void
P2::editorView()
{
  if (!_showEditorView)
    return;
  ImGui::Begin("Editor View Settings");
  editorViewGui();
  ImGui::End();
}

inline void
P2::fileMenu()
{
  if (ImGui::MenuItem("New"))
  {
    // TODO
  }
  if (ImGui::MenuItem("Open...", "Ctrl+O"))
  {
    // TODO
  }
  ImGui::Separator();
  if (ImGui::MenuItem("Save", "Ctrl+S"))
  {
    // TODO
  }
  if (ImGui::MenuItem("Save As..."))
  {
    // TODO
  }
  ImGui::Separator();
  if (ImGui::MenuItem("Exit", "Alt+F4"))
  {
    shutdown();
  }
}

inline bool
showStyleSelector(const char* label)
{
  static int style = 1;

  if (!ImGui::Combo(label, &style, "Classic\0Dark\0Light\0"))
    return false;
  switch (style)
  {
    case 0: ImGui::StyleColorsClassic();
      break;
    case 1: ImGui::StyleColorsDark();
      break;
    case 2: ImGui::StyleColorsLight();
      break;
  }
  return true;
}

inline void
P2::showOptions()
{
  ImGui::PushItemWidth(ImGui::GetWindowWidth() * 0.6f);
  showStyleSelector("Color Theme##Selector");
  ImGui::ColorEdit3("Selected Wireframe", _selectedWireframeColor);
  ImGui::ColorEdit3("Selected Frustum Frame", _selectedFrustumframeColor);
  ImGui::PopItemWidth();
}

inline void
P2::mainMenu()
{
  if (ImGui::BeginMainMenuBar())
  {
    if (ImGui::BeginMenu("File"))
    {
      fileMenu();
      ImGui::EndMenu();
    }
    if (ImGui::BeginMenu("View"))
    {
      if (Camera::current() == 0)
        ImGui::MenuItem("Edit View", nullptr, true, false);
      else
      {
        static const char* viewLabels[]{"Editor", "Renderer"};

        if (ImGui::BeginCombo("View", viewLabels[_viewMode]))
        {
          for (auto i = 0; i < IM_ARRAYSIZE(viewLabels); ++i)
          {
            if (ImGui::Selectable(viewLabels[i], _viewMode == i))
              _viewMode = (ViewMode)i;
          }
          ImGui::EndCombo();
        }
      }
      ImGui::Separator();
      ImGui::MenuItem("Assets Window", nullptr, &_showAssets);
      ImGui::MenuItem("Editor View Settings", nullptr, &_showEditorView);
      ImGui::EndMenu();
    }
    if (ImGui::BeginMenu("Tools"))
    {
      if (ImGui::BeginMenu("Options"))
      {
        showOptions();
        ImGui::EndMenu();
      }
      ImGui::EndMenu();
    }
    ImGui::EndMainMenuBar();
  }
}

void
P2::gui()
{
  mainMenu();
  hierarchyWindow();
  inspectorWindow();
  assetsWindow();
  editorView();
}

void
P2::drawHierarchy(SceneObject& node)
{
  for (const auto& child : node.children())
  {
    if (!child->visible)
      continue;

    for (const auto& component : child->components())
    {
      if (auto p = dynamic_cast<Primitive*>(component.get()))
      {
        drawPrimitive(*p);
      }
      else if (auto c = dynamic_cast<Camera*>(component.get()))
      {
        if (child == _current) // Only draw frustum if is it's in inspection mode
          drawCamera(*c);
      }

      if (child == _current)
      {
        auto t = child->transform();
        _editor->drawAxes(t->position(), mat3f{ t->rotation() });
      }
    }

    if (child->hasChildren())
      drawHierarchy(*child);
  }
}

inline void
P2::drawPrimitive(Primitive& primitive)
{
  auto m = glMesh(primitive.mesh());

  if (nullptr == m)
    return;

  auto t = primitive.transform();
  auto normalMatrix = mat3f{ t->worldToLocalMatrix() }.transposed();

  _program.setUniformMat4("transform", t->localToWorldMatrix());
  _program.setUniformMat3("normalMatrix", normalMatrix);
  _program.setUniformVec4("color", primitive.color);
  _program.setUniform("flatMode", (int)0);
  
  m->bind();
  
  GLRenderer::drawMesh(m, GL_FILL);
  
  if (primitive.sceneObject() != _current)
    return;
  
  _program.setUniformVec4("color", _selectedWireframeColor);
  _program.setUniform("flatMode", (int)1);
  
  GLRenderer::drawMesh(m, GL_LINE);
}

inline void
P2::drawCamera(Camera& camera)
{
  // Capture NEAR and FAR values
  float F, B;
  camera.clippingPlanes(F, B);

  // Adjust FAR scale, keep only 20% from original
  B *= 0.2f;

  const auto Mm = camera.cameraToWorldMatrix();

  // NEAR (F - Front) - FAR (B - Back)
  vec3f pNTL, pNTR, pNBL, pNBR, pFTL, pFTR, pFBL, pFBR;

  if (camera.projectionType() == Camera::ProjectionType::Parallel)
  {
    const auto top = camera.height() * 0.5f;
    const auto right = top * camera.aspectRatio();

    pNTL = { -right,  top, -F };
    pNTR = {  right,  top, -F };
    pNBL = { -right, -top, -F };
    pNBR = {  right, -top, -F };
    
    pFTL = { -right,  top, -B };
    pFTR = {  right,  top, -B };
    pFBL = { -right, -top, -B };
    pFBR = {  right, -top, -B };
  }
  else if (camera.projectionType() == Camera::ProjectionType::Perspective)
  {
    const auto angle = camera.viewAngle();
    const auto t = (float) tan(math::toRadians(angle) * 0.5);

    const auto nearTop = F * t;
    const auto farTop = B * t;

    const auto nearRight = nearTop * camera.aspectRatio();
    const auto farRight = farTop * camera.aspectRatio();

    pNTL = { -nearRight,  nearTop, -F };
    pNTR = {  nearRight,  nearTop, -F };
    pNBL = { -nearRight, -nearTop, -F };
    pNBR = {  nearRight, -nearTop, -F };

    pFTL = { -farRight,  farTop, -B };
    pFTR = {  farRight,  farTop, -B };
    pFBL = { -farRight, -farTop, -B };
    pFBR = {  farRight, -farTop, -B };
  }
  else
  {
    return; // Just to make sure...
  }

  // Apply transformation to points from local coord to global
  pNTL = Mm.transform(pNTL);
  pNTR = Mm.transform(pNTR);
  pNBL = Mm.transform(pNBL);
  pNBR = Mm.transform(pNBR);

  pFTL = Mm.transform(pFTL);
  pFTR = Mm.transform(pFTR);
  pFBL = Mm.transform(pFBL);
  pFBR = Mm.transform(pFBR);

  // Set the color to frutum from Tools
  _editor->setLineColor(_selectedFrustumframeColor);

  // Draw square to NEAR (FRONT)
  _editor->drawLine(pNTL, pNTR);
  _editor->drawLine(pNTL, pNBL);
  _editor->drawLine(pNBL, pNBR);
  _editor->drawLine(pNBR, pNTR);

  // Draw square to FAR (BACK)
  _editor->drawLine(pFTL, pFTR);
  _editor->drawLine(pFTL, pFBL);
  _editor->drawLine(pFBL, pFBR);
  _editor->drawLine(pFBR, pFTR);

  // Draw lines to connect NEAR to FAR
  _editor->drawLine(pNTL, pFTL);
  _editor->drawLine(pNTR, pFTR);
  _editor->drawLine(pNBL, pFBL);
  _editor->drawLine(pNBR, pFBR);
}

void
P2::drawPreview(Camera& camera)
{
  const auto scaledWidth = width() / 4;
  const auto scaledHeight = height() / 4;
  const auto center = (width() / 2) - scaledWidth / 2;

  int tmpVp[4];
  glGetIntegerv(GL_VIEWPORT, tmpVp);                 // Store original viewPort
  glViewport(center, 10, scaledWidth, scaledHeight); // Set viewPort to 1/4 original scale
  glEnable(GL_SCISSOR_TEST);                         // Enable openGL to scissor mode
  glScissor(center, 10, scaledWidth, scaledHeight);  // Set openGL to "cut" the new viewPort choosen

  // Draw normally
  _renderer->setCamera(&camera);
  _renderer->setImageSize(width(), height());
  _renderer->render();
  _program.use();

  glDisable(GL_SCISSOR_TEST);                         // Disable scissor area
  glViewport(tmpVp[0], tmpVp[1], tmpVp[2], tmpVp[3]); // Rollback viewPort to original values
}

inline void
P2::renderScene()
{
  if (auto camera = Camera::current())
  {
    _renderer->setCamera(camera);
    _renderer->setImageSize(width(), height());
    _renderer->render();
    _program.use();
  }
  else
  {
    // Clear buffer in case there's no camera selected (current)
    GLRenderer::clearScreenWith(_scene->backgroundColor);
  }
}

constexpr auto CAMERA_RES = 0.01f;
constexpr auto ZOOM_SCALE = 1.01f;

void
P2::render()
{
  if (_viewMode == ViewMode::Renderer)
  {
    renderScene();
    return;
  }
  if (_moveFlags)
  {
    const auto delta = _editor->orbitDistance() * CAMERA_RES;
    auto d = vec3f::null();

    if (_moveFlags.isSet(MoveBits::Forward))
      d.z -= delta;
    if (_moveFlags.isSet(MoveBits::Back))
      d.z += delta;
    if (_moveFlags.isSet(MoveBits::Left))
      d.x -= delta;
    if (_moveFlags.isSet(MoveBits::Right))
      d.x += delta;
    if (_moveFlags.isSet(MoveBits::Up))
      d.y += delta;
    if (_moveFlags.isSet(MoveBits::Down))
      d.y -= delta;
    _editor->pan(d);
  }

  _editor->newFrame();
  drawHierarchy(*_scene->root());

  if (auto p = dynamic_cast<SceneObject*>(_current))
  {
    for (const auto& component : p->components())
    {
      if (auto camera = dynamic_cast<Camera*>(component.get()))
      {
        drawPreview(*camera);
      }
    }
  }
}

bool
P2::windowResizeEvent(int width, int height)
{
  _editor->camera()->setAspectRatio(float(width) / float(height));
  _renderer->camera()->setAspectRatio(float(width) / float(height));
  
  return true;
}

bool
P2::keyInputEvent(int key, int action, int mods)
{
  if (action == GLFW_PRESS && key == GLFW_KEY_F && mods == GLFW_MOD_ALT)
  {
    if (auto scene = dynamic_cast<Scene*>(_current))
    {
      _editor->setDefaultView();
    }
    else if (auto object = dynamic_cast<SceneObject*>(_current))
    {
      auto camera = _editor->camera();
      auto pos = object->transform()->position();
      auto scale = object->transform()->localScale();
      
      // Set global position - 10 distance (to not set the camera "inside" the object)
      auto zoom = vec3f(scale[0] + 5, scale[1] + 5, scale[2] + 5);
      camera->transform()->setPosition(pos + zoom);

      // TODO adjust rotation
    }
    return false;
  }

  auto active = action != GLFW_RELEASE && mods == GLFW_MOD_ALT;
  switch (key)
  {
    case GLFW_KEY_W:
      _moveFlags.enable(MoveBits::Forward, active);
      break;
    case GLFW_KEY_S:
      _moveFlags.enable(MoveBits::Back, active);
      break;
    case GLFW_KEY_A:
      _moveFlags.enable(MoveBits::Left, active);
      break;
    case GLFW_KEY_D:
      _moveFlags.enable(MoveBits::Right, active);
      break;
    case GLFW_KEY_Q:
      _moveFlags.enable(MoveBits::Up, active);
      break;
    case GLFW_KEY_E:
      _moveFlags.enable(MoveBits::Down, active);
      break;
  }

  return false;
}

bool
P2::scrollEvent(double, double yOffset)
{
  if (ImGui::GetIO().WantCaptureMouse || _viewMode == ViewMode::Renderer)
    return false;
  _editor->zoom(yOffset < 0 ? 1.0f / ZOOM_SCALE : ZOOM_SCALE);
  return true;
}

bool
P2::mouseButtonInputEvent(int button, int actions, int mods)
{
  if (ImGui::GetIO().WantCaptureMouse || _viewMode == ViewMode::Renderer)
    return false;
  (void)mods;

  auto active = actions == GLFW_PRESS;

  if (button == GLFW_MOUSE_BUTTON_RIGHT)
    _dragFlags.enable(DragBits::Rotate, active);
  else if (button == GLFW_MOUSE_BUTTON_MIDDLE)
    _dragFlags.enable(DragBits::Pan, active);
  if (_dragFlags)
    cursorPosition(_pivotX, _pivotY);
  return true;
}

bool
P2::mouseMoveEvent(double xPos, double yPos)
{
  if (!_dragFlags)
    return false;
  _mouseX = (int)xPos;
  _mouseY = (int)yPos;

  const auto dx = (_pivotX - _mouseX);
  const auto dy = (_pivotY - _mouseY);

  _pivotX = _mouseX;
  _pivotY = _mouseY;
  if (dx != 0 || dy != 0)
  {
    if (_dragFlags.isSet(DragBits::Rotate))
    {
      const auto da = -_editor->camera()->viewAngle() * CAMERA_RES;
      isKeyPressed(GLFW_KEY_LEFT_ALT) ?
        _editor->orbit(dy * da, dx * da) :
        _editor->rotateView(dy * da, dx * da);
    }
    if (_dragFlags.isSet(DragBits::Pan))
    {
      const auto dt = -_editor->orbitDistance() * CAMERA_RES;
      _editor->pan(-dt * math::sign(dx), dt * math::sign(dy), 0);
    }
  }
  return true;
}
