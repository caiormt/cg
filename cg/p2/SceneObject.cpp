//[]---------------------------------------------------------------[]
//|                                                                 |
//| Copyright (C) 2018, 2019 Orthrus Group.                         |
//|                                                                 |
//| This software is provided 'as-is', without any express or       |
//| implied warranty. In no event will the authors be held liable   |
//| for any damages arising from the use of this software.          |
//|                                                                 |
//| Permission is granted to anyone to use this software for any    |
//| purpose, including commercial applications, and to alter it and |
//| redistribute it freely, subject to the following restrictions:  |
//|                                                                 |
//| 1. The origin of this software must not be misrepresented; you  |
//| must not claim that you wrote the original software. If you use |
//| this software in a product, an acknowledgment in the product    |
//| documentation would be appreciated but is not required.         |
//|                                                                 |
//| 2. Altered source versions must be plainly marked as such, and  |
//| must not be misrepresented as being the original software.      |
//|                                                                 |
//| 3. This notice may not be removed or altered from any source    |
//| distribution.                                                   |
//|                                                                 |
//[]---------------------------------------------------------------[]
//
// OVERVIEW: SceneObject.cpp
// ========
// Source file for scene object.
//
// Author(s): Paulo Pagliosa (and your name)
// Last revision: 07/09/2019

#include "SceneObject.h"
#include "Scene.h"

namespace cg
{ // begin namespace cg


/////////////////////////////////////////////////////////////////////
//
// SceneObject implementation
// ===========
void
SceneObject::setParent(SceneObject* parent)
{
  // parent nullptr to remove this parent
  if (parent == nullptr)
  {
    if (this->parent() != nullptr)
    {
      _parent->removeChild(this);
      _parent = nullptr;
    }
  }
  else
  {
    // I'm root!
    if (this->parent() == nullptr)
    {
      _parent = makeUse(parent);
      _parent->addChild(this);
    }
    // changing parent
    else
    {
      Reference oldParent = this->parent();
      if (oldParent.get() == parent) return;

      oldParent->removeChild(this);
      _parent = makeUse(parent);
      _parent->addChild(this);
    }
  }

  this->transform()->parentChanged();
}

void
SceneObject::addComponent(Component* component)
{
  if (component == nullptr) return;

  component->_sceneObject = this;
  bool exists = std::find_if(_components.begin(), _components.end(), [component](const auto& c) -> bool { return c->typeName() == component->typeName(); }) != _components.end();
  if (!exists) // Checks if it already has a component with the same type;
  {
    _components.push_back(makeUse(component));
  }
}

void
SceneObject::removeComponent(Component* component)
{
  _components.remove(component);
}

void
SceneObject::addChild(SceneObject* child)
{
  _children.push_back(makeUse(child));
}

void
SceneObject::removeChild(SceneObject* child)
{
  _children.remove(child);
}

} // end namespace cg
