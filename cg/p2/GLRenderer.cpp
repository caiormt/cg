//[]---------------------------------------------------------------[]
//|                                                                 |
//| Copyright (C) 2018, 2019 Orthrus Group.                         |
//|                                                                 |
//| This software is provided 'as-is', without any express or       |
//| implied warranty. In no event will the authors be held liable   |
//| for any damages arising from the use of this software.          |
//|                                                                 |
//| Permission is granted to anyone to use this software for any    |
//| purpose, including commercial applications, and to alter it and |
//| redistribute it freely, subject to the following restrictions:  |
//|                                                                 |
//| 1. The origin of this software must not be misrepresented; you  |
//| must not claim that you wrote the original software. If you use |
//| this software in a product, an acknowledgment in the product    |
//| documentation would be appreciated but is not required.         |
//|                                                                 |
//| 2. Altered source versions must be plainly marked as such, and  |
//| must not be misrepresented as being the original software.      |
//|                                                                 |
//| 3. This notice may not be removed or altered from any source    |
//| distribution.                                                   |
//|                                                                 |
//[]---------------------------------------------------------------[]
//
// OVERVIEW: GLRenderer.cpp
// ========
// Source file for OpenGL renderer.
//
// Author(s): Paulo Pagliosa (and your name)
// Last revision: 09/09/2019

#include "GLRenderer.h"
#include "Primitive.h"

namespace cg
{ // begin namespace cg

// Forward definition
inline void
drawHierarchy(GLSL::Program& program, SceneObject& node);

inline void
drawPrimitive(GLSL::Program& program, Primitive& primitive);

//////////////////////////////////////////////////////////
//
// GLRenderer implementation
// ==========
void
GLRenderer::update()
{
  Renderer::update();
  // TODO
}

void
GLRenderer::render()
{
  const auto& bc = _scene->backgroundColor;
  GLRenderer::clearScreenWith(bc);

  const auto& p = _camera->transform()->position();
  auto vp = vpMatrix(_camera);

 _program->setUniformMat4("vpMatrix", vp);
 _program->setUniformVec4("ambientLight", _scene->ambientLight);
 _program->setUniformVec3("lightPosition", p);

 drawHierarchy(*_program, *_scene->root());
}

inline void
drawHierarchy(GLSL::Program& program, SceneObject& node)
{
  for (const auto& child : node.children())
  {
    if (!child->visible)
      continue;

    for (const auto& component : child->components())
    {
      if (auto p = dynamic_cast<Primitive*>(component.get()))
      {
        drawPrimitive(program, *p);
      }
    }

    if (child->hasChildren())
      drawHierarchy(program, *child);
  }
}

inline void
drawPrimitive(GLSL::Program& program, Primitive& primitive)
{
  auto m = glMesh(primitive.mesh());

  if (nullptr == m)
    return;

  auto t = primitive.transform();
  auto normalMatrix = mat3f{ t->worldToLocalMatrix() }.transposed();

  program.setUniformMat4("transform", t->localToWorldMatrix());
  program.setUniformMat3("normalMatrix", normalMatrix);
  program.setUniformVec4("color", primitive.color);
  program.setUniform("flatMode", (int)0);
 
  m->bind();
  
  GLRenderer::drawMesh(m, GL_FILL);
}
} // end namespace cg
