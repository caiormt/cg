# Computa��o Gr�fica (T01) - 2019

## Alunos
- Caio Riyousuke Miyada Tokunaga (2017.1904.002-8)
- Rodrigo Schio Wengenroth Silva (2017.1904.001-0)

## Atividades

### Adicionado
- A1 - Implementado desenho de linhas para mostrar o frustum da camera.
  -  Utilizado escala de 20% do valor original do Back (FAR)
- A2 - Implementado modo vista de renderiza��o utilizando a c�mera corrente.
  - Caso n�o exista c�mera corrente, n�o renderiza nada.
- A3 - Implementado janela de preview da cena
  - Adicionado preview ao centro inferior da tela.
- A4 - Implementado modo foco da c�mera utilizando atalho Alt+F.
  - Ajusta a c�mera do editor para a posi��o objeto atualmente selecionado.
  - Ideal seria calcular uma certa dist�ncia do objeto e ajustar a rota��o da c�mera do editor.
    - Da forma implementada, a c�mera est� ficando "dentro" dos objetos.
- A5 - Implementado mecanismo de troca de pai
  - Adicionado Drag n' Drop dos elementos no menu de hierarquia, permitindo o arrasto do um elemento para "dentro" de outro.
    - N�o � poss�vel mover um n� para "dentro" de seus "filhos" e/ou "netos".
  - � poss�vel mover entre objetos e adicionar como ra�z da cena livremente.
  - Adicionado trava para n�o autorizar a muda�a de escala menor que 0.001.
- A6 - Implementado mecanismo para adicionar objetos de cena e componentes.
  - Implementado mecanismo para adicionar objetos de cena
  - Implementado mecanismo para adicionar componentes � um objeto de cena.
    - N�o � poss�vel adicionar um componente do mesmo tipo.

### Extra
- Adicionado op��o de altera��o de cor das linhas do "frustum" no menu de Op��es.


### Observa��es
- A6 - N�o implementado mecanismo de remo��o de Objetos de Cena e Componentes.
  - N�o implementado devido a algum Reference mantendo um ponteiro nulo para um Objeto de Cena / Componente (ainda n�o detectado).