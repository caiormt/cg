//[]---------------------------------------------------------------[]
//|                                                                 |
//| Copyright (C) 2018 Orthrus Group.                               |
//|                                                                 |
//| This software is provided 'as-is', without any express or       |
//| implied warranty. In no event will the authors be held liable   |
//| for any damages arising from the use of this software.          |
//|                                                                 |
//| Permission is granted to anyone to use this software for any    |
//| purpose, including commercial applications, and to alter it and |
//| redistribute it freely, subject to the following restrictions:  |
//|                                                                 |
//| 1. The origin of this software must not be misrepresented; you  |
//| must not claim that you wrote the original software. If you use |
//| this software in a product, an acknowledgment in the product    |
//| documentation would be appreciated but is not required.         |
//|                                                                 |
//| 2. Altered source versions must be plainly marked as such, and  |
//| must not be misrepresented as being the original software.      |
//|                                                                 |
//| 3. This notice may not be removed or altered from any source    |
//| distribution.                                                   |
//|                                                                 |
//[]---------------------------------------------------------------[]
//
// OVERVIEW: Primitive.cpp
// ========
// Source file for primitive.
//
// Author(s): Paulo Pagliosa (and your name)
// Last revision: 30/10/2018

#include "Primitive.h"
#include "Transform.h"
#include "Intersection.h"

namespace cg
{ // begin namespace cg

inline constexpr auto
epsilon()
{
  return 1e-8f;
}

bool
Primitive::intersect(const Ray& ray, Intersection& hit) const
{
  if (_mesh == nullptr)
    return false;

  auto t = const_cast<Primitive*>(this)->transform();
  Ray localRay{ ray, t->worldToLocalMatrix() };
  float tMin, tMax;

  if (_mesh->bounds().intersect(localRay, tMin, tMax))
  {
    const auto data = _mesh->data();
    
    // Brute Force
    for (auto index = 0; index < data.numberOfTriangles; index++)
    {
      const auto triangle = data.triangles[index];
      const auto p0 = data.vertices[triangle.v[0]];
      const auto p1 = data.vertices[triangle.v[1]];
      const auto p2 = data.vertices[triangle.v[2]];

      const auto e1 = p1 - p0;
      const auto e2 = p2 - p0;
      const auto s1 = localRay.direction.cross(e2);

      const auto O = s1.dot(e1);
      if (fabs(O) < epsilon()) continue; // Not intercept
      const auto invD = math::inverse(O);

      const auto s  = localRay.origin - p0;
      const auto s2 = s.cross(e1);
      const auto tl = s2.dot(e2) * invD;
      if (tl < 0) continue; // We cross but in the inverse direction.

      const auto b1 = s1.dot(s) * invD;
      if (b1 < 0) continue; // Outside of the triangle

      const auto b2 = s2.dot(localRay.direction) * invD;
      if (b2 < 0) continue; // Outside of the triangle

      const auto b0 = 1 - b1 - b2;
      if (b0 < 0) continue; // Outside of the triangle

      // Convert the local distance to the global distance.
      const auto M = t->localToWorldMatrix();
      const auto D = M.transformVector(localRay.direction);
      const auto distance = tl * D.length();

      // Finally! We intersect, check if we already found any triangle closest.
      if (distance < hit.distance)
      {
        hit.object = this;
        hit.triangleIndex = index;
        hit.distance = distance;
        hit.p = vec3f(b0, b1, b2); // Baricentric coordinates
        hit.userData = _mesh->userData;
      }
    }
    // Return if we intersect any triangle
    return hit.object != nullptr;
  }
  return false;
}

} // end namespace cg
