#version 330 core

struct Light
{
  vec3 position;
  vec3 direction;
  vec4 color;
  int type;
  int falloff;
  float apexAngle;
  int falloffFactor;
};

uniform mat4 transform;
uniform mat3 normalMatrix;
uniform mat4 vpMatrix = mat4(1);
uniform int flatMode;

// Material properties
uniform vec4 ambientLight = vec4(0.2, 0.2, 0.2, 1);
uniform vec4 ambientColor;
uniform vec4 color;
uniform vec4 spotColor;
uniform float spotExp;

// Lights
uniform Light Lights[10];
uniform int lightsCounter;

// Camera
uniform vec3 cameraPosition;

layout(location = 0) in vec4 position;
layout(location = 1) in vec3 normal;

out vec4 vertexColor;

void main()
{
  vec4 P = transform * position;
  vec3 N = normalize(normalMatrix * normal);
  vec3 V = normalize(vec3(P) - cameraPosition);
  vec4 A = ambientLight * float(1 - flatMode) * ambientColor;

  vec4 finalColor = vec4(0);

  for (int i = 0; i < lightsCounter; i++)
  {
    // Directional
    if (Lights[i].type == 0)
    {
      vec4 I = Lights[i].color;
      vec3 L = Lights[i].direction;
      vec3 R = reflect(L, N);
      vec4 D = color * I * max(dot(-N, L), float(flatMode));
      vec4 S = spotColor * I * pow(max(dot(-R, V), float(flatMode)), spotExp);
      finalColor += D + S; 
    }
    // Point
    else if (Lights[i].type == 1)
    {
      vec3 Ll = vec3(P) - Lights[i].position;
      float distance = length(Ll);
      float decay = pow(distance, Lights[i].falloff);
      
      vec4 I = Lights[i].color / decay;
      vec3 L = Ll / distance;
      vec3 R = reflect(L, N);
      vec4 D = color * I * max(dot(-N, L), float(flatMode));
      vec4 S = spotColor * I * pow(max(dot(-R, V), float(flatMode)), spotExp);
      finalColor += D + S; 
    }
    // Spot
    else if (Lights[i].type == 2)
    {
      vec3 Ll = vec3(P) - Lights[i].position;
      vec3 Dl = Lights[i].direction;
      float distance = length(Ll);
      vec3 L = Ll / distance;

      float cossin = dot(Dl, L);
      if (Lights[i].apexAngle <= cossin)
      {
        float decay = pow(distance, Lights[i].falloff);
        vec4 I = (Lights[i].color / decay) * pow(cossin, Lights[i].falloffFactor);
        vec3 R = reflect(L, N);
        vec4 D = color * I * max(dot(-N, L), float(flatMode));
        vec4 S = spotColor * I * pow(max(dot(-R, V), float(flatMode)), spotExp);
        finalColor += D + S; 
      }
    }
  }

  gl_Position = vpMatrix * P;
  vertexColor = A + finalColor;
}
