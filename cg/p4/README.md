# Computação Gráfica (T01) - 2019

## Alunos
- Caio Riyousuke Miyada Tokunaga (2017.1904.002-8)
- Rodrigo Schio Wengenroth Silva (2017.1904.001-0)

## Atividades

### Adicionado
- A1 - Implementado intersect do RayTracer
  - RayTracer invoca intersect da BVH.
  - Ajusta a distância global.
  - Mantém apenas o Intersection mais próximo da origem do raio.
- A2 - Implementado intersect no Primitive
  - Na classe Primitive, o intersect foi implementado utilizando força bruta (iterando todos triângulos).
  - Este método é invocado apenas no P4.cpp para a detecção de qual é o primitivo na seleção com o click do mouse.
- A3 - Implementado intersect na BVH
  - Na classe BVH, o intersect foi implementado conforme especificação.
  - Caso haja interseção com o Bounding Box, verificar:
    - Caso seja folha, testar seus triangulos
    - Caso contrário, adicionar os dois filhos na pilha.
  - Manter apenas o triângulo mais próximo da origem.
- A4 - Implementado tonalização de Phong e implementação de raios secundários.
  - Verificar se o ponto está na sombra.
    - Caso a luz seja Pontual ou Spot, a distância máxima do raio é a distânia entre o ponto e a luz.
  - Utilizando Phong para a tonalização de luz direta.
- A5 - Implementado duas cenas de exemplos além da cena inicial.
  - A cena "Spheres" demonstra a reflexão de esferas em duas esferas centrais agindo como espelhos.
  - A cena "Glass Box" demonstra a reflexão recursiva de um F16 entre dois espelhos.
  - Ambas imagens geradas estão no projeto nomeados de "f16.png" e "spheres.png".

### Observações
- Corrigido normalização da normal interpolada no shader de fragmento do P3.
