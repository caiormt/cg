//[]---------------------------------------------------------------[]
//|                                                                 |
//| Copyright (C) 2018, 2019 Orthrus Group.                         |
//|                                                                 |
//| This software is provided 'as-is', without any express or       |
//| implied warranty. In no event will the authors be held liable   |
//| for any damages arising from the use of this software.          |
//|                                                                 |
//| Permission is granted to anyone to use this software for any    |
//| purpose, including commercial applications, and to alter it and |
//| redistribute it freely, subject to the following restrictions:  |
//|                                                                 |
//| 1. The origin of this software must not be misrepresented; you  |
//| must not claim that you wrote the original software. If you use |
//| this software in a product, an acknowledgment in the product    |
//| documentation would be appreciated but is not required.         |
//|                                                                 |
//| 2. Altered source versions must be plainly marked as such, and  |
//| must not be misrepresented as being the original software.      |
//|                                                                 |
//| 3. This notice may not be removed or altered from any source    |
//| distribution.                                                   |
//|                                                                 |
//[]---------------------------------------------------------------[]
//
// OVERVIEW: RayTracer.cpp
// ========
// Source file for simple ray tracer.
//
// Author(s): Paulo Pagliosa (and your name)
// Last revision: 16/10/2019

#include "Camera.h"
#include "RayTracer.h"
#include "Primitive.h"
#include "Light.h"
#include <time.h>
#include <queue>

using namespace std;

namespace cg
{ // begin namespace cg

void
printElapsedTime(const char* s, clock_t time)
{
  printf("%sElapsed time: %.4f s\n", s, (float)time / CLOCKS_PER_SEC);
}


/////////////////////////////////////////////////////////////////////
//
// RayTracer implementation
// =========
RayTracer::RayTracer(Scene& scene, Camera* camera):
  Renderer{scene, camera},
  _maxRecursionLevel{6},
  _minWeight{MIN_WEIGHT}
{
  // TODO: BVH
}

void
RayTracer::render()
{
  throw std::runtime_error("RayTracer::render() invoked");
}

inline float
windowHeight(Camera* c)
{
  if (c->projectionType() == Camera::Parallel)
    return c->height();
  return c->nearPlane() * tan(math::toRadians(c->viewAngle() * 0.5f)) * 2;

}
void
RayTracer::renderImage(Image& image)
{
  auto t = clock();
  const auto& m = _camera->cameraToWorldMatrix();

  // VRC axes
  _vrc.u = m[0];
  _vrc.v = m[1];
  _vrc.n = m[2];
  // init auxiliary mapping variables
  _W = image.width();
  _H = image.height();
  _Iw = math::inverse(float(_W));
  _Ih = math::inverse(float(_H));

  auto height = windowHeight(_camera);

  _W >= _H ? _Vw = (_Vh = height) * _W * _Ih : _Vh = (_Vw = height) * _H * _Iw;
  
  // init pixel ray
  _pixelRay.origin = _camera->transform()->position();
  _pixelRay.direction = -_vrc.n;
  _camera->clippingPlanes(_pixelRay.tMin, _pixelRay.tMax);
  
  // Debug info
  _numberOfRays = _numberOfHits = 0;

  // Fill temporary strucure
  fillComponents();

  // Start rendering
  scan(image);

  // Cleaning temporary structure
  _primitives.clear();
  _lights.clear();

  printf("\nNumber of rays: %llu", _numberOfRays);
  printf("\nNumber of hits: %llu", _numberOfHits);
  printElapsedTime("\nDONE! ", clock() - t);
}

inline void
RayTracer::fillComponents()
{
  // Init helper structure
  auto queue = std::queue<Reference<SceneObject>>();
  queue.push(_scene->root());

  // Process while have elements left
  while (!queue.empty())
  {
    // Seek the first element
    Reference node = queue.front();

    // Seek for the primitive or light
    for (const auto& component : node->components())
    {
      if (auto p = dynamic_cast<Primitive*>(component.get()))
      {
        const auto mesh = p->mesh();
        
        // Without mesh, skip it
        if (mesh == nullptr) continue;
        
        _primitives.push_back(p);

        if (bvhMap[mesh] == nullptr)
          bvhMap[mesh] = new BVH{ *mesh, 16 };
      }
      else if (auto l = dynamic_cast<Light*>(component.get()))
      {
        _lights.push_back(l);
      }
    }

    // Add each child to queue for further processing
    for (const auto& child : node->children())
    {
      // If not visible, remove from processing
      if (child->visible)
        queue.push(child);
    }

    // Remove processed item
    queue.pop();
  }
}

void
RayTracer::setPixelRay(float x, float y)
//[]---------------------------------------------------[]
//|  Set pixel ray                                      |
//|  @param x coordinate of the pixel                   |
//|  @param y cordinates of the pixel                   |
//[]---------------------------------------------------[]
{
  auto p = imageToWindow(x, y);

  switch (_camera->projectionType())
  {
    case Camera::Perspective:
      _pixelRay.direction = (p - _camera->nearPlane() * _vrc.n).versor();
      break;

    case Camera::Parallel:
      _pixelRay.origin = _camera->transform()->position() + p;
      break;
  }
}

void
RayTracer::scan(Image& image)
{
  ImageBuffer scanLine{_W, 1};

  for (int j = 0; j < _H; j++)
  {
    auto y = (float)j + 0.5f;

    printf("Scanning line %d of %d\r", j + 1, _H);
    for (int i = 0; i < _W; i++)
      scanLine[i] = shoot((float)i + 0.5f, y);
    image.setData(0, j, scanLine);
  }
}

Color
RayTracer::shoot(float x, float y)
//[]---------------------------------------------------[]
//|  Shoot a pixel ray                                  |
//|  @param x coordinate of the pixel                   |
//|  @param y cordinates of the pixel                   |
//|  @return RGB color of the pixel                     |
//[]---------------------------------------------------[]
{
  // set pixel ray
  setPixelRay(x, y);

  // trace pixel ray
  Color color = trace(_pixelRay, 0, 1.0f);

  // adjust RGB color
  if (color.r > 1.0f)
    color.r = 1.0f;
  if (color.g > 1.0f)
    color.g = 1.0f;
  if (color.b > 1.0f)
    color.b = 1.0f;
  // return pixel color
  return color;
}

Color
RayTracer::trace(const Ray& ray, uint32_t level, float weight)
//[]---------------------------------------------------[]
//|  Trace a ray                                        |
//|  @param the ray                                     |
//|  @param recursion level                             |
//|  @param ray weight                                  |
//|  @return color of the ray                           |
//[]---------------------------------------------------[]
{
  if (level > _maxRecursionLevel)
    return Color::black;
  _numberOfRays++;

  Intersection hit;

  return intersect(ray, hit) ? shade(ray, hit, level, weight) : background();
}

inline constexpr auto
rt_eps()
{
  return 1e-4f;
}

bool
RayTracer::intersect(const Ray& ray, Intersection& hit)
//[]---------------------------------------------------[]
//|  Ray/object intersection                            |
//|  @param the ray (input)                             |
//|  @param information on intersection (output)        |
//|  @return true if the ray intersects an object       |
//[]---------------------------------------------------[]
{
  auto found{ false };

  // Clean resources
  hit.object = nullptr;
  hit.distance = ray.tMax;

  // Code for brute force
  // // Iterate over primitives
  // for (const auto& p : _primitives)
  // {
  //   found |= p->intersect(ray, hit);
  // }

  // Code for BVH
  for (const auto& p : _primitives)
  {
    const auto mesh = p->mesh();
    const auto t = p->transform();
    
    // Transform the Ray to LocalRay
    Ray localRay{ ray, t->worldToLocalMatrix() };
  
    // Convert the local distance to the global distance.
    const auto updateDistance = [t, localRay](float tl) {
      const auto M = t->localToWorldMatrix();
      const auto D = M.transformVector(localRay.direction);
      return tl * D.length();
    };
  
    const auto intersect = bvhMap[mesh]->intersect(localRay, hit, updateDistance);
    hit.object = intersect ? p : hit.object;
    found |= intersect;
  }

  return found;
}

static inline 
vec3f reflect(vec3f direction, vec3f normal)
{
  return direction - 2 * (normal.dot(direction) * normal);
}

static inline
float maxComponent(const Color& c)
{
  return std::max(c.r, std::max(c.g, c.b));
}

Color
RayTracer::shade(const Ray& ray, Intersection& hit, int level, float weight)
//[]---------------------------------------------------[]
//|  Shade a point P                                    |
//|  @param the ray (input)                             |
//|  @param information on intersection (input)         |
//|  @param recursion level                             |
//|  @param ray weight                                  |
//|  @return color at point P                           |
//[]---------------------------------------------------[]
{
  // Const values
  const auto o = hit.object->sceneObject();
  const auto t = o->transform();
  const auto m = hit.object->material;
  const auto cameraPosition = _camera->transform()->position();
  const auto normalMatrix = mat3f{ t->worldToLocalMatrix() }.transposed();
  
  // Data from the triangle
  const auto data = hit.object->mesh()->data();
  const auto triangle = data.triangles[hit.triangleIndex];
  
  // Interpolated normal
  auto normal = vec3f::null();
  for (auto i = 0; i < 3; i++)
    normal += hit.p[i] * data.vertexNormals[triangle.v[i]];

  // Ambient color
  const auto ambient = _scene->ambientLight * m.ambient;

  // Original calculated values
  const auto Po = ray(hit.distance);
  const auto No = (normalMatrix * normal).versor();

  // Check if the normal is "pointing" to outside
  const auto N = math::isPositive(ray.direction.dot(No)) ? -No : No;

  // Foward P with some constant to avoid cross itself.
  const auto P = Po + rt_eps() * N;
  const auto V = (P - cameraPosition).versor();

  // Initial color - the ambient color
  auto color{ ambient };

  // For each light
  for (const auto& l : _lights)
  {
    const auto pos = l->transform()->position();
    const auto rot = l->transform()->rotation();
    const auto dir = rot * vec3f::up();

    // Light direction
    const auto direction = l->type() == Light::Type::Directional ? dir : P - pos;

    // Create Ray to direction of the Light.
    auto lightRay = Ray{ P, -direction };

    // Change the max distance hit
    // Do not consider intersected by primitive after the light
    if (l->type() != Light::Type::Directional)
      lightRay.tMax = direction.length();

    // Point in shadow, this light is unreachable
    if (shadow(lightRay)) continue;

    if (l->type() == Light::Type::Directional)
    {
      const auto I = l->color;
      const auto L = direction.versor();
      const auto R = reflect(L, N);
      const auto D = m.diffuse * I * max(-N.dot(L), 0.0f);
      const auto S = m.spot * I * pow(max(-R.dot(V), 0.0f), m.shine);
      color += D + S;
    }
    else if (l->type() == Light::Type::Point)
    {
      const auto Ll = direction;
      const auto distance = Ll.length();
      const auto decay = pow(distance, (int)l->falloff());

      const auto I = l->color * math::inverse(decay);
      const auto L = Ll * math::inverse(distance);
      const auto R = reflect(L, N);
      const auto D = m.diffuse * I * max(-N.dot(L), 0.0f);
      const auto S = m.spot * I * pow(max(-R.dot(V), 0.0f), m.shine);
      color += D + S;
    }
    else if (l->type() == Light::Type::Spot)
    {
      const auto Ll = direction;
      const auto Dl = dir.versor();
      const auto distance = Ll.length();
      const auto L = Ll * math::inverse(distance);

      const auto lcossin = cos(math::toRadians(l->openingAngle()));
      const auto cossin = Dl.dot(L);
      if (lcossin <= cossin)
      {
        const auto decay = pow(distance, (int)l->falloff());
        const auto I = (l->color * math::inverse(decay)) * pow(cossin, (int)l->falloffFactor());
        const auto R = reflect(L, N);
        const auto D = m.diffuse * I * max(-N.dot(L), 0.0f);
        const auto S = m.spot * I * pow(max(-R.dot(V), 0.0f), m.shine);
        color += D + S;
      }
    }
  }

  // Secondary ray
  if (m.specular != Color::black)
  {
    // Check if the material is polished enough
    const auto w = weight * maxComponent(m.specular);
    if (w > _minWeight)
    {
      // Create new ray and add its color
      const auto R = reflect(ray.direction, N);
      const auto reflectionRay = Ray{ P, R };
      color += m.specular * trace(reflectionRay, level + 1, w);
    }
  }

  return color;
}

Color
RayTracer::background() const
//[]---------------------------------------------------[]
//|  Background                                         |
//|  @return background color                           |
//[]---------------------------------------------------[]
{
  return _scene->backgroundColor;
}

bool
RayTracer::shadow(const Ray& ray)
//[]---------------------------------------------------[]
//|  Verifiy if ray is a shadow ray                     |
//|  @param the ray (input)                             |
//|  @return true if the ray intersects an object       |
//[]---------------------------------------------------[]
{
  Intersection hit;
  return intersect(ray, hit);
}

} // end namespace cg
