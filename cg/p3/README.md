# Computa��o Gr�fica (T01) - 2019

## Alunos
- Caio Riyousuke Miyada Tokunaga (2017.1904.002-8)
- Rodrigo Schio Wengenroth Silva (2017.1904.001-0)

## Atividades

### Adicionado
- A1 - Completado classe Light
  - Adicionado edi��o das propriedades de acordo com o tipo de luz.
  - Restri��o do �ngulo de abertura quando Spot (entre 0 e 90 graus).
- A2 - Implementado fun��es para adicionar novas Luzes na Cena.
  - Possibilidade de criar as luzes como objeto de cena ou componente.
- A3 - Implementado drawLight para desenhar as luzes.
  - Considerando que todas as luzes iniciam com dire��o do eixo Y (vec3::up).
  - Caso seja direcional, desenhado 3 vetores paralelos na dire��o da luz.
    - Considerando a origem dos vetores na posi��o atual do objeto de cena (apenas para visualiza��o).
  - Caso seja pontual, desenhando 4 c�rculos na posi��o da luz
    - Considerando a dire��o do eixo Z da c�mera do editor como a normal dos c�rculos (manter sempre "frente" � visualiza��o).
  - Caso seja spot, desenhando o cone representando a "luz de abajur".
    - Desenhando um vetor no centro do cone em dire��o da luz.
    - Desenhando um c�rculo a uma certa dist�ncia (tamanho do vetor) reprensentando o "alcance"
    - Desenhando linhas conectando a origem da luz ao c�rculo.
- A4 - Implementado tonaliza��o de Gourard para o modo edi��o.
- A5 - Implementado tonaliza��o de Phong para o modo preview e render.
- A6 - Implementado cena inicial (hard coded) para demonstra��o.
  - Considerando 3 luzes (uma de cada tipo)
  - Considerando 2 caixas
  - Camera do editor e c�mera principal colocadas no mesmo local para demonstrar a diferen�a entre os tonalizantes.

### Extra
- Adicionado op��o de desenhar as arestas de todos os primitivos.
  - Considerar cor das arestas do objeto selecionado priorit�rio sobre a cor das arestas do modo edi��o.


### Observa��es
- Corrigido remo��o de objetos de cena e componentes do P2.
- Corrigido foco do objeto selecionado (atalho Alt+F).