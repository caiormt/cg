//[]---------------------------------------------------------------[]
//|                                                                 |
//| Copyright (C) 2018, 2019 Orthrus Group.                         |
//|                                                                 |
//| This software is provided 'as-is', without any express or       |
//| implied warranty. In no event will the authors be held liable   |
//| for any damages arising from the use of this software.          |
//|                                                                 |
//| Permission is granted to anyone to use this software for any    |
//| purpose, including commercial applications, and to alter it and |
//| redistribute it freely, subject to the following restrictions:  |
//|                                                                 |
//| 1. The origin of this software must not be misrepresented; you  |
//| must not claim that you wrote the original software. If you use |
//| this software in a product, an acknowledgment in the product    |
//| documentation would be appreciated but is not required.         |
//|                                                                 |
//| 2. Altered source versions must be plainly marked as such, and  |
//| must not be misrepresented as being the original software.      |
//|                                                                 |
//| 3. This notice may not be removed or altered from any source    |
//| distribution.                                                   |
//|                                                                 |
//[]---------------------------------------------------------------[]
//
// OVERVIEW: Light.h
// ========
// Class definition for light.
//
// Author(s): Paulo Pagliosa (and your name)
// Last revision: 14/10/2019

#ifndef __Light_h
#define __Light_h

#include "Component.h"
#include "graphics/Color.h"

namespace cg
{ // begin namespace cg


/////////////////////////////////////////////////////////////////////
//
// Light: light class
// =====
class Light: public Component
{
public:
  enum Type
  {
    Directional,
    Point,
    Spot
  };

  enum Falloff
  {
    None,
    Linear,
    Quadratic
  };

  Color color{ Color::white };

  Light(): 
    Light(Light::Type::Directional)
  {
    // do nothing
  }

  Light(Light::Type type):
    Component{ "Light" },
    _type{ type },
    _falloffType{ Light::Falloff::Linear },
    _openingAngle{ 45.0f },
    _falloffFactor{ Light::Falloff::Linear }
  {
    //do nothing
  }

  auto type() const
  {
    return _type;
  }

  void setType(Type type)
  {
    _type = type;
  }

  auto falloff() const
  {
    return _falloffType;
  }

  void setFalloff(Light::Falloff type)
  {
    _falloffType = type;
  }

  auto falloffFactor() const
  {
    return _falloffFactor;
  }

  void setFalloffFactor(Light::Falloff factor)
  {
    _falloffFactor = factor;
  }

  auto openingAngle() const
  {
    return _openingAngle;
  }

  void setOpeningAngle(float angle)
  {
    _openingAngle = angle;
  }

private:
  Type _type;
  Light::Falloff _falloffType;
  Light::Falloff _falloffFactor;
  float _openingAngle;

}; // Light

} // end namespace cg

#endif // __Light_h
