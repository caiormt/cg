//[]---------------------------------------------------------------[]
//|                                                                 |
//| Copyright (C) 2018, 2019 Orthrus Group.                         |
//|                                                                 |
//| This software is provided 'as-is', without any express or       |
//| implied warranty. In no event will the authors be held liable   |
//| for any damages arising from the use of this software.          |
//|                                                                 |
//| Permission is granted to anyone to use this software for any    |
//| purpose, including commercial applications, and to alter it and |
//| redistribute it freely, subject to the following restrictions:  |
//|                                                                 |
//| 1. The origin of this software must not be misrepresented; you  |
//| must not claim that you wrote the original software. If you use |
//| this software in a product, an acknowledgment in the product    |
//| documentation would be appreciated but is not required.         |
//|                                                                 |
//| 2. Altered source versions must be plainly marked as such, and  |
//| must not be misrepresented as being the original software.      |
//|                                                                 |
//| 3. This notice may not be removed or altered from any source    |
//| distribution.                                                   |
//|                                                                 |
//[]---------------------------------------------------------------[]
//
// OVERVIEW: GLRenderer.cpp
// ========
// Source file for OpenGL renderer.
//
// Author(s): Paulo Pagliosa (and your name)
// Last revision: 09/09/2019

#include "GLRenderer.h"
#include "Primitive.h"
#include "Light.h"

namespace cg
{ // begin namespace cg


//////////////////////////////////////////////////////////
//
// GLRenderer implementation
// ==========
void
GLRenderer::update()
{
  Renderer::update();
  // TODO
}

inline void
GLRenderer::drawPrimitive(Primitive& primitive)
{
  auto m = glMesh(primitive.mesh());

  if (nullptr == m)
    return;

  auto t = primitive.transform();
  auto normalMatrix = mat3f{ t->worldToLocalMatrix() }.transposed();

  _program->setUniformMat4("transform", t->localToWorldMatrix());
  _program->setUniformMat3("normalMatrix", normalMatrix);
  _program->setUniform("flatMode", (int)0);
  _program->setUniformVec4("ambientColor", primitive.material.ambient);
  _program->setUniformVec4("color", primitive.material.diffuse);
  _program->setUniformVec4("spotColor", primitive.material.spot);
  _program->setUniform("spotExp", primitive.material.shine);

  m->bind();

  GLRenderer::drawMesh(*m, GL_FILL);
}

inline void
GLRenderer::drawLight(Light& light)
{
  if (_lightsCounter > 9) return;

  const auto transform = light.sceneObject()->transform();
  const auto name = std::string("Lights[") + std::to_string(_lightsCounter) + std::string("]");

  _lightsCounter += 1;

  _program->setUniformVec4((name + std::string(".color")).c_str(), light.color);
  _program->setUniform((name + std::string(".type")).c_str(), (int)light.type());

  if (light.type() == Light::Type::Directional)
  {
    const auto rotation  = transform->rotation();
    const auto direction = rotation * vec3f::up();

    _program->setUniformVec3((name + std::string(".direction")).c_str(), direction);
  }
  else if (light.type() == Light::Type::Point)
  {
    const auto position = transform->position();

    _program->setUniformVec3((name + std::string(".position")).c_str(), position);
    _program->setUniform((name + std::string(".falloff")).c_str(), (int)light.falloff());

  }
  else if (light.type() == Light::Type::Spot)
  {
    const auto position  = transform->position();
    const auto rotation  = transform->rotation();
    const auto normal    = rotation * vec3f::up();
    const auto randAngle = math::toRadians(light.openingAngle());

    _program->setUniformVec3((name + std::string(".position")).c_str(), position);
    _program->setUniformVec3((name + std::string(".direction")).c_str(), normal);
    _program->setUniform((name + std::string(".falloff")).c_str(), (int)light.falloff());
    _program->setUniform((name + std::string(".apexAngle")).c_str(), cos(randAngle));
    _program->setUniform((name + std::string(".falloffFactor")).c_str(), (int)light.falloffFactor());
  }
}

inline void
GLRenderer::drawHierarchy(SceneObject& node)
{
  for (const auto& child : node.children())
  {
    if (!child->visible)
      continue;

    for (const auto& component : child->components())
    {
      if (auto p = dynamic_cast<Primitive*>(component.get()))
      {
        drawPrimitive(*p);
      } 
      else if (auto l = dynamic_cast<Light*>(component.get()))
      {
        drawLight(*l);
      }
    }

    if (child->hasChildren())
      drawHierarchy(*child);
  }
}

void
GLRenderer::render()
{
  const auto& bc = _scene->backgroundColor;
  GLRenderer::clearScreenWith(bc);

  const auto& p = _camera->transform()->position();
  auto vp = vpMatrix(_camera);

  _program->use();
  _program->setUniformMat4("vpMatrix", vp);
  _program->setUniformVec4("ambientLight", _scene->ambientLight);
  _program->setUniformVec3("cameraPosition", p);

  _lightsCounter = 0; // Reset before rendering
  drawHierarchy(*_scene->root());
  _program->setUniform("lightsCounter", _lightsCounter);
}

} // end namespace cg
