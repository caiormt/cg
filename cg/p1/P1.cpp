#include "P1.h"

namespace cg
{ // begin namespace cg

  inline Primitive*
  makeBoxMesh()
  {
    const vec4f p1{ -0.5, -0.5, -0.5, 1 };
    const vec4f p2{ +0.5, -0.5, -0.5, 1 };
    const vec4f p3{ +0.5, +0.5, -0.5, 1 };
    const vec4f p4{ -0.5, +0.5, -0.5, 1 };
    const vec4f p5{ -0.5, -0.5, +0.5, 1 };
    const vec4f p6{ +0.5, -0.5, +0.5, 1 };
    const vec4f p7{ +0.5, +0.5, +0.5, 1 };
    const vec4f p8{ -0.5, +0.5, +0.5, 1 };
    const Color c1{ Color::black };
    const Color c2{ Color::red };
    const Color c3{ Color::yellow };
    const Color c4{ Color::green };
    const Color c5{ Color::blue };
    const Color c6{ Color::magenta };
    const Color c7{ Color::cyan };
    const Color c8{ Color::white };

    // Box vertices
    static const vec4f v[]
    {
      p1, p5, p8, p4, // x = -0.5
      p2, p3, p7, p6, // x = +0.5
      p1, p2, p6, p5, // y = -0.5
      p4, p8, p7, p3, // y = +0.5
      p1, p4, p3, p2, // z = -0.5
      p5, p6, p7, p8  // z = +0.5
    };

    // Box vertex colors
    static const Color c[]
    {
      c1, c5, c8, c4, // x = -0.5
      c2, c3, c7, c6, // x = +0.5
      c1, c2, c6, c5, // y = -0.5
      c4, c8, c7, c3, // y = +0.5
      c1, c4, c3, c2, // z = -0.5
      c5, c6, c7, c8  // z = +0.5
    };

    // Box triangles
    static const GLMeshArray::Triangle t[]
    {
      { 0,  1,  2}, { 2,  3,  0},
      { 4,  5,  7}, { 5,  6,  7},
      { 8,  9, 11}, { 9, 10, 11},
      {12, 13, 14}, {14, 15, 12},
      {16, 17, 19}, {17, 18, 19},
      {20, 21, 22}, {22, 23, 20}
    };

    return new Primitive{ new GLMeshArray{24, {v, 0}, {c, 1}, 12, t} };
  }

} // end namespace cg

inline void
P1::buildScene()
{
  _current = _scene = new Scene{ "Scene 1" };

  auto root1 = new SceneObject{ "Box 1", _scene };
  root1->setParent(nullptr);
  root1->addComponent(makeBoxMesh());

  auto root2 = new SceneObject{ "Box 2", _scene };
  root2->setParent(nullptr);
  root2->addComponent(makeBoxMesh());

  auto root3 = new SceneObject{ "Object 1", _scene };
  root3->setParent(nullptr);

  auto nested1 = new SceneObject{ "Box 1", _scene };
  nested1->setParent(root1);
  nested1->addComponent(makeBoxMesh());

  auto nested2 = new SceneObject{ "Object 1", _scene };
  nested2->setParent(root1);
}

void
P1::initialize()
{
  Application::loadShaders(_program, "p1.vs", "p1.fs");
  buildScene();
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_POLYGON_OFFSET_FILL);
  glPolygonOffset(1.0f, 1.0f);
  glLineWidth(2.0f);
  glEnable(GL_LINE_SMOOTH);
  _program.use();
}

namespace ImGui
{
  void ShowDemoWindow(bool*);
}

static void
RecursivelyRenderHierarchy(SceneNode* node, SceneNode** current)
{
  auto children = node->children();
  for (auto it = children.begin(); it != children.end(); it++)
  {
    auto child = *it;

    ImGuiTreeNodeFlags flag = child->hasChildren() ?
      ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick :
      ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen;

    auto open = ImGui::TreeNodeEx(
      child,
      *current == child ? flag | ImGuiTreeNodeFlags_Selected : flag,
      child->name()
    );

    if (ImGui::IsItemClicked())
      (*current) = child;

    if (child->hasChildren() && open) {
      RecursivelyRenderHierarchy(child, current);
      ImGui::TreePop();
    }
  }
}

inline void
P1::hierarchyWindow()
{
  ImGui::Begin("Hierarchy");
  if (ImGui::Button("Create###object"))
    ImGui::OpenPopup("CreateObjectPopup");
  if (ImGui::BeginPopup("CreateObjectPopup"))
  {
    if (ImGui::MenuItem("Empty Object"))
    {
      auto name = "Object " + std::to_string(_current->nextObjectSeq());
      auto newBox = new SceneObject{ name.c_str(), _scene };

      if (dynamic_cast<Scene*>(_current))
      {
        newBox->setParent(nullptr);
      }
      else if (dynamic_cast<SceneObject*>(_current))
      {
        newBox->setParent((SceneObject*) _current);
      }
      else
      {
        // WHAT TO DO, PANIC?
        delete newBox;
      }
    }

    if (ImGui::BeginMenu("3D Object"))
    {
      if (ImGui::MenuItem("Box"))
      {
        auto name = "Box " + std::to_string(_current->nextBoxSeq());
        auto newBox = new SceneObject{ name.c_str(), _scene };
        newBox->addComponent(makeBoxMesh());

        if (dynamic_cast<Scene*>(_current))
        {
          newBox->setParent(nullptr);
        }
        else if (dynamic_cast<SceneObject*>(_current))
        {
          newBox->setParent((SceneObject*) _current);
        }
        else
        {
          // WHAT TO DO, PANIC?
          delete newBox;
        }
      }
      ImGui::EndMenu();
    }
    ImGui::EndPopup();
  }
  ImGui::Separator();

  ImGuiTreeNodeFlags flag{ ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick };
  auto open =
    ImGui::TreeNodeEx(
      _scene,
      _current == _scene ? flag | ImGuiTreeNodeFlags_Selected : flag,
      _scene->name()
    );

  if (ImGui::IsItemClicked())
    _current = _scene;

  if (open)
  {
    RecursivelyRenderHierarchy(_scene, &_current);
    ImGui::TreePop();
  }

  ImGui::End();
}

namespace ImGui
{ // begin namespace ImGui

  void
  ObjectNameInput(SceneNode* object)
  {
    const int bufferSize{ 128 };
    static SceneNode* current;
    static char buffer[bufferSize];

    if (object != current)
    {
      strcpy_s(buffer, bufferSize, object->name());
      current = object;
    }
    if (ImGui::InputText("Name", buffer, bufferSize))
      object->setName(buffer);
  }

  inline bool
  ColorEdit3(const char* label, Color& color)
  {
    return ImGui::ColorEdit3(label, (float*)& color);
  }

  inline bool
  DragVec3(const char* label, vec3f& v)
  {
    return DragFloat3(label, (float*)& v, 0.1f, 0.0f, 0.0f, "%.2g");
  }

  void
  TransformEdit(Transform* transform)
  {
    vec3f temp;

    temp = transform->localPosition();
    if (ImGui::DragVec3("Position", temp))
      transform->setLocalPosition(temp);
    temp = transform->localEulerAngles();
    if (ImGui::DragVec3("Rotation", temp))
      transform->setLocalEulerAngles(temp);
    temp = transform->localScale();
    if (ImGui::DragVec3("Scale", temp))
      transform->setLocalScale(temp);
  }

} // end namespace ImGui

inline void
P1::sceneGui()
{
  auto scene = (Scene*)_current;

  ImGui::ObjectNameInput(_current);
  ImGui::Separator();
  if (ImGui::CollapsingHeader("Colors"))
  {
    ImGui::ColorEdit3("Background", backgroundColor);
    ImGui::ColorEdit3("Selected Wireframe", selectedWireframeColor);
  }
}

static inline void
RenderPrimitiveGuiInfo(Component* component)
{
  if (ImGui::CollapsingHeader(component->typeName()))
  {
    auto primitive = (Primitive*) component;
    auto mesh = primitive->mesh();

    ImGui::Text("Vertices: %d", mesh->vertexCount());
    ImGui::Separator();
  }
}

inline void
P1::sceneObjectGui()
{
  auto object = (SceneObject*) _current;

  ImGui::ObjectNameInput(object);
  ImGui::SameLine();
  ImGui::Checkbox("###visible", &object->visible);
  ImGui::Separator();

  if (ImGui::CollapsingHeader(object->transform()->typeName()))
  {
    auto t = object->transform();
    ImGui::TransformEdit(t);
  }

  auto components = object->components();
  for (auto it = components.begin(); it != components.end(); it++)
  {
    auto component = *it;
    if (dynamic_cast<Primitive*>(component))
      RenderPrimitiveGuiInfo(component);
  }
}

inline void
P1::objectGui()
{
  if (_current == nullptr)
    return;
  if (dynamic_cast<SceneObject*>(_current))
  {
    sceneObjectGui();
    return;
  }
  if (dynamic_cast<Scene*>(_current))
    sceneGui();
}

inline void
P1::inspectorWindow()
{
  ImGui::Begin("Inspector");
  objectGui();
  ImGui::End();
}

void
P1::gui()
{
  hierarchyWindow();
  inspectorWindow();
  /*
  static bool demo = true;
  ImGui::ShowDemoWindow(&demo);
  */
}

static inline void
RenderPrimitive(
  GLSL::Program& program,
  Color& wireFrameColor, 
  Component* component, 
  bool isSelected)
{
  auto primitive = (Primitive*) component;
  auto m = primitive->mesh();

  auto transform = primitive->transform()->localToWorldMatrix();
  program.setUniformMat4("transform", transform);

  m->bind();
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glDrawElements(GL_TRIANGLES, m->vertexCount(), GL_UNSIGNED_INT, 0);

  if (!isSelected) return;

  m->setVertexColor(wireFrameColor);
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  glDrawElements(GL_TRIANGLES, m->vertexCount(), GL_UNSIGNED_INT, 0);
  m->useVertexColors();
}

static void
RecursivelyRender(
  GLSL::Program& program,
  Color& wireFrameColor, 
  SceneNode* node, 
  SceneNode* current)
{
  auto nodes = node->children();
  for (auto it = nodes.begin(); it != nodes.end(); it++)
  {
    auto node = *it;

    if (dynamic_cast<SceneObject*>(node))
    {
      auto sceneObject = (SceneObject*)node;
      if (!sceneObject->visible) continue;

      auto components = sceneObject->components();
      for (auto itc = components.begin(); itc != components.end(); itc++)
      {
        auto component = *itc;

        if (dynamic_cast<Primitive*>(component))
        {
          auto isSelected = current == sceneObject;
          RenderPrimitive(program, wireFrameColor, component, isSelected);
        }
      }

      if (node->hasChildren())
        RecursivelyRender(program, wireFrameColor, node, current);
    }
  }
}

void
P1::render()
{
  GLWindow::render();  
  RecursivelyRender(_program, selectedWireframeColor, _scene, _current);
}

/// Button codes
const auto DELETE_BUTTON = 261;

bool
P1::keyInputEvent(int key, int action, int mods)
{
  if (action == GLFW_PRESS)
  {
    if (key == DELETE_BUTTON)
    {
      (void)mods;
      if (dynamic_cast<Scene*>(_current))
      {
        // do nothing
        return false;
      }

      if (dynamic_cast<SceneObject*>(_current))
      {
        auto object = (SceneObject*) _current;
        auto parent = object->parent();

        if (parent == nullptr) // I'm root!
        {
          _scene->removeChild(object);
          _current = _scene;
        }
        else
        {
          parent->removeChild(object);
          _current = parent;
        }

        delete object;
        return true;
      }
    }
  }
  return false;
}