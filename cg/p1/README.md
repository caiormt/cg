# Computa��o Gr�fica (T01) - 2019

## Alunos
- Caio Riyousuke Miyada Tokunaga (2017.1904.002-8)
- Rodrigo Schio Wengenroth Silva (2017.1904.001-0)

## Atividades

### Adicionado
- A1 - Adicionado lista de filhos no Scene e SceneObject
  - Uma vez que ambos Scene e SceneObject derivam de SceneNode, implementamos a lista de filhos nesta classe base.
  - Utilizamos a implementa��o padr�o de lista (std::list), pois oferece m�todos de remo��o, iterador e adicionar na cabe�a / cauda.
- A2 - Implementado m�todo setParent
  - Quando receber nullptr, enviar mensagem a Scene para ser adicionado como raiz.
  - Quando receber ponteiro para SceneObject, enviar mensagem ao parent atual para ser removido e ao novo parent para ser adicionado.
- A3 - Adicionado lista de Components
  - Utilizamos a implementa��o padr�o de lista (std::list), pois oferece m�todos de remo��o, iterador e adicionar na cabe�a / cauda.
  - Adicionado no construtor de SceneObject, a cria��o do "\_transform{}" e adicionado na lista de "\_components{}"
- A4 - Ajustado a implementa��o corrente para a estrutura hier�rquica.
  - Adicionado novos SceneObjects na raiz com setParent(nullptr) e aninhados com setParent(&parent);
- A5 - Implementado
  - Quando n� possui filhos, renderiza �cone e permite duplo clique para expandir.
  - Quando � folha, n�o possui nenhum comportamento adicional. 
- A6 - Implementado
  - Itera na lista de componentes de cada SceneObject, identifica se possui um component primitive.
    - Caso positivo, renderiza a quantidade de v�rtices.
    - Caso negativo, n�o renderiza informa��o sobre.
- A7 - Implementado
  - Renderiza todos os SceneObjects que est�o vis�veis
    - Caso o objeto n�o est� vis�vel, o objeto e todos os seus filhos n�o s�o renderizados. 
  - Checa se o Primitive da itera��o � o atual
    - Caso positivo, adiciona as cores do Wireframe 


### Extra
- Implementado keyInputEvent, ao pressionar o bot�o "DELETE"
  - O n� atual (selecionado) � apagado juntamente com seus componentes e filhos.
    - Implementado m�todo destrutor no SceneObject para a exclus�o dos seus componentes e filhos.
  - Caso o n� atual seja ra�z, o n� selecionado passa a ser Scene.
  - Caso contr�rio, o n� selecionado passa a ser o Parent. 