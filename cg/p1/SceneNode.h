//[]---------------------------------------------------------------[]
//|                                                                 |
//| Copyright (C) 2018 Orthrus Group.                               |
//|                                                                 |
//| This software is provided 'as-is', without any express or       |
//| implied warranty. In no event will the authors be held liable   |
//| for any damages arising from the use of this software.          |
//|                                                                 |
//| Permission is granted to anyone to use this software for any    |
//| purpose, including commercial applications, and to alter it and |
//| redistribute it freely, subject to the following restrictions:  |
//|                                                                 |
//| 1. The origin of this software must not be misrepresented; you  |
//| must not claim that you wrote the original software. If you use |
//| this software in a product, an acknowledgment in the product    |
//| documentation would be appreciated but is not required.         |
//|                                                                 |
//| 2. Altered source versions must be plainly marked as such, and  |
//| must not be misrepresented as being the original software.      |
//|                                                                 |
//| 3. This notice may not be removed or altered from any source    |
//| distribution.                                                   |
//|                                                                 |
//[]---------------------------------------------------------------[]
//
// OVERVIEW: SceneNode.h
// ========
// Class definition for generic scene node.
//
// Author(s): Paulo Pagliosa (and your name)
// Last revision: 25/08/2018

#ifndef __SceneNode_h
#define __SceneNode_h

#include "core/SharedObject.h"
#include <string>
#include <list>

namespace cg
{ // begin namespace cg


/////////////////////////////////////////////////////////////////////
//
// SceneNode: generic scene node class
// =========
class SceneNode: public SharedObject
{
public:
  /// Returns the name of this scene node.
  auto name() const
  {
    return _name.c_str();
  }

  /// Returns the scene objects of this scene node.
  auto children() const
  {
    return _children;
  }

  /// Returns if this SceneNode has children nested.
  auto hasChildren() const
  {
    return !_children.empty();
  }

  /// Sequence generator of the next child Object
  auto nextObjectSeq()
  {
    _objectSeq++;
    return _objectSeq;
  }

  /// Sequence generator of the next child Box
  auto nextBoxSeq()
  {
    _boxSeq++;
    return _boxSeq;
  }

  /// Sets the name of this scene node.
  void setName(const char* format, ...);

  /// Add the given SceneObject as child of this
  void addChild(SceneNode* child);

  /// Remove the given SceneObject
  void removeChild(SceneNode* child);

protected:
  SceneNode() = default;
  std::list<SceneNode*> _children;

  SceneNode(const char* name):
    _name{name},
    _children{}
  {
    // do nothing
  }

private:
  int _objectSeq{ 0 };
  int _boxSeq{ 0 };
  std::string _name;

}; // SceneNode

} // end namespace cg

#endif // __SceneNode_h
